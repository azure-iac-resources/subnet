# Create subnet
module "subnet" {
  source = "git::https://gitlab.com/azure-iac2/root-modules.git//subnet"
  subnet_name          = var.subnet_name
  business_divsion     = var.business_divsion
  environment          = var.environment
  resource_group_name  = data.azurerm_resource_group.rg.name
  vnet_name            = data.azurerm_virtual_network.vnet.name
  sn_address_space     = var.sn_address_space
}